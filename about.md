---
layout: page
title: About
permalink: /about/
---

<!-- <amp-img width="700" height="700" layout="fixed"
src="/assets/images/daniel2018.jpg"></amp-img> -->

My name is Daniel Heitmann, but some people call me dictvm, which is pronounced
like the latin word [dictum](https://en.wikipedia.org/wiki/Dictum). You don't
have to, though.

I am a Systems Engineer from Düsseldorf InVision. I am also a cyclist. I'm
slightly addicted to coffee, Black Metal and roadbikes. I also love my [Maine
Coon cats](https://photos.app.goo.gl/OMpSrjSZCvIC1Huf2).

In the last couple of years I've stopped smoking (4 years), stopped drinking alcohol (3 years) and I'm vegan (1 year).
