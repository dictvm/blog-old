layout: post
title: 'Global Warming and Cows'
date: '2017-MM-DD HH:MM:00'

I don't want to be the annoying vegan and I still think that you should eat whatever you enjoy, but [this piece](https://gizmodo.com/do-cow-farts-actually-contribute-to-global-warming-1562144730) by Gizmodo is a good read, no matter your opinion on the subject.

> Some farms have experimented with having their livestock live in a plastic bubble, which takes the expelled gas and converts it into electricity. But this process is both expensive, inefficient, and considered somewhat inhumane, forcing animals to live inside an artificial bubble.

I'm pretty disgusted by this idea.