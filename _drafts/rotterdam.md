---
layout: post
title: Blog reboot
date: '2017-09-04 11:19:49'
---
When I moved away from Münster in 2008 I already dreamt of living in another country at some point in time. Maybe when I was 30, I thought. I’ve just turned 30 a few weeks ago. I’m still living in Düsseldorf right now. But I’m thinking about where I want to live in a couple of years.

For reasons unknown  to me, I always imagined Rotterdam to be this dirty, decrepit city, with an old industrialized harbor, somewhat like a very ugly brother of Hamburg. I was completely wrong. I haven’t seen such a nice, liveable city in a long time - and I’ve had my share of travels into european cities. I’ve been to Warsaw, Amsterdam, Berlin, Istanbul, London and Dublin in the past years and I enjoyed my stay in all of these cities, except for the obvious lack of infrastructure in Dublin. In direct comparison, Rotterdam feels a lot more modern and thought-through. Which makes sense, considering that the city officials decided to not just rebuild the old buildings after they were bombed down in WW2. Instead, they invited architects to erect more modern buildings and to consider how a modern city would look like.
Bicycle infrastructure
I’m a bicyclist and I’m accustomed to vehicular warfare on the streets of Düsseldorf. During  my stay, I haven’t had a single issue in Rotterdam and that says a lot, because my bike is my only way of transport. Everyone is riding their bicycle while texting, wearing full headphones or having a group conversation at a pace of 15-30km/h. Never have I felt this safe and respected on my bicycle as in Rotterdam. I expect Amsterdam to be roughly on the same level, but I haven’t yet rented a bike there, to see for myself. 
Nightlife
This city never sleeps. I am not much of a party-person, but I can’t imagine that you 
Diversity
During all my travels, never before have I seen a city with such a diversity. People of all colors and all classes just interact with each other during everyday life. Whenever a conservative right-wring schmuck ever yells that multiculturalism doesn’t work, they should be forced to go on holiday here for a week. 
WiFi
I’m a sucker for open WiFi. And Rotterdam delivers. While about 50% are behind an annoying captive portal, at least they exist and provide decent bandwidth. Though just talking to people helps and during my stay I have convinced two café owners to reconsider and open their WiFi to the general public instead of forcing people to post advertising to social media before being allowed to use their internet.
Temperature
I was hoping that the summers were a bit more chilly in the Netherlands. But I’ve had to suffer through 30C for the past 3 days and I’m just not cut out for these temperatures. Anything between 22-25C is fine. At least it gets better when I’m on my bike. 
Coffee
This is the primary disadvantage of Rotterdam. It’s coffee. Wherever I go to, the coffee is always great. I would lose a lot of money in cafés, if I moved here. This also isn’t a funny attempt at describing the other speciality of the Netherlands, which are coffee shops. I actually haven’t visited one of them yet.
Cinema
When I was younger, I never went to the Cinema. It was expensive and watching movies illegally at home was way cooler. But I’ve come to like the cinema. I still don’t buy their popcorn or nachos because it’s overly expensive, but at least I’m paying for the movies now, right? Well, Rotterdam is one of those cities with an IMAX cinema. In Germany I’d have to travel to Berlin to get to experience that. IMAX means that the 

