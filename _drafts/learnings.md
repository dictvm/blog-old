---
layout: post
title: Blog reboot
date: '2017-09-04 11:19:49'

---
* Don't hire people who pride themselves in being better alone
* If you did hire someone like that, let them go within a reasonable timeframe
* Think about what you alert on
* Have on-call shifts, never alert via broadcast
* If your team members complain about alert fatigue, fix the alerts
* You're probably not a special snowflake, your problems aren't unique
* Don't spend more than a year in a company that doesn't make you happy
* Don't accept egomaniacal behavior from anyone
* Don't try to deliver a product you don't believe in
* If you're sure that something won't work, prove it, then point to it
* Without upper management you will not be able to drive change
* Cultural changes require a lot of patience
* Don't collect todos, have a backlog, clean up things you won't do or that have become irrelevant
* Find consensus on the amount of technical debt you're willing to accept to ship something
* Delegate as much as possible
* Have people accept responsibility for certain tasks that need to be done (e.g. to-do cleanup)
